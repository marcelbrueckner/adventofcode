# Advent of Code

My solutions so far for the [Advent of Code], an Advent calendar of small programming puzzles.

## Repository structure

You can find my solutions (or current working status in some cases) to the Advent of Code puzzles in the folders corresponding to the respective year. They in turn contain a subfolder for each language in which I have tried to find a solution. Since there are usually also input values beside the code, there is - for reasons of clarity - another subfolder for each day of the advent calendar. If you're lucky, there are also tests written so you can try and verify your own implementation.

The solution in Ruby for the 2nd day of the 2019 Advent of Code would therefore be at [`2019/ruby/2019-12-02`](2019/ruby/2019-12-02). An possibly existing test would be found at [`2019/ruby/2019-12-02/spec`](2019/ruby/2019-12-02/spec).

### Initialization

Of course manually creating the structure as described above is tedious, there's a small script called `puzzles.py` doing the bootstrapping work. It parses the Advent of Code's webpage for a given date range and fetches the puzzle description(s) into a README.md file for each day a puzzle is available (using [pypandoc](https://pypi.org/project/pypandoc/) which needs a working [pandoc](https://pandoc.org/) installation).
