#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse, os, pytz, requests, sys, time
from bs4 import BeautifulSoup
from datetime import datetime
from dateutil.parser import parse
from dateutil.tz import tzlocal

# Initialize variables
now = datetime.now(tzlocal())
tz_local = now.tzname()
today = parse(f"{now.year}-{now.month}-{now.day}")
today_local = pytz.timezone(tz_local).localize(parse(f"{today.year}-{today.month}-{today.day}"), is_dst=None)
today_est = pytz.timezone("EST").localize(parse(f"{today.year}-{today.month}-{today.day} 01:00 AM"), is_dst=None)

# https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-terminal-in-python
class bcolors:
  HEADER = '\033[95m'
  OKBLUE = '\033[94m'
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  BOLD = '\033[1m'
  UNDERLINE = '\033[4m'

# Arguments
parser = argparse.ArgumentParser(description='Fetch Advent of Code\'s puzzle description(s)', add_help=False)

pagroup_date = parser.add_argument_group('Dates', 'Please be aware that this script only makes a single request to AoC approx. every 5 seconds, so your time interval shouldn\'t be too large if you don\'t want to wait for so long.')
pagroup_date.add_argument('-y', '--year', 
  help='Fetch available puzzle descriptions of a year\'s whole advent season. When used together with --date-*, the overlapping date interval is used.',
  type=int, choices=range(2015, today.year + 1 if today.month == 12 else today.year)
)
pagroup_date.add_argument('-f', '--date-start', help='Fetch available puzzle descriptions starting from the specified date. Defaults to today if --year isn\'t specified explicitly. When used together with --year, the overlapping date interval is used.')
pagroup_date.add_argument('-t', '--date-end', help='Fetch available puzzle descriptions up to the specified date (inclusive). Defaults to today if --year isn\'t specified explicitly. When used together with --year, the overlapping date interval is used.')

pagroup_lang = parser.add_argument_group('Language', 'Programming language option')
pagroup_lang.add_argument('-l', '--language', help='Language for which a subfolder should be created. If empty or unspecified, no subfolder will be created.')

pagroup_help = parser.add_argument_group('Help', 'Get help')
pagroup_help.add_argument('-h', '--help', action='help', help='Show this help message and exit.')

args = parser.parse_args()

# Consoli"date" :)
if args.date_start is None:
  date_start = today
else:
  date_start = parse(args.date_start)

  if date_start > today:
    print(
      f"{bcolors.WARNING}"
      f"Start date must not be in the future but is {args.date_start} while today is {today.strftime('%Y-%m-%d')}.\n"
      f"Will use corrected start date {today.strftime('%Y-%m-%d')} (today) instead.\n"
      f"{bcolors.ENDC}"
    )
    date_start = today

if args.date_end is None:
  date_end = today
else:
  date_end = parse(args.date_end)

  if date_start > date_end:
    print(
      f"{bcolors.FAIL}"
      f"Start date must not be greater than end date but is {date_start.strftime('%Y-%m-%d')} while end date is {date_end.strftime('%Y-%m-%d')}.\n"
      f"{bcolors.ENDC}"
    )
    sys.exit(1)

if args.year is None:
  if args.date_start is None and args.date_end is None:
    year = today.year
else:
  year = args.year
  if year not in range(date_start.year, date_end.year + 1):
    print(
      f"{bcolors.FAIL}"
      f"Specified year {year} does not overlap start and end date ({date_start.strftime('%Y-%m-%d')} - {date_end.strftime('%Y-%m-%d')})."
      f"{bcolors.ENDC}"
    )
    sys.exit(1)

# Fetch information from remote for the specified/calculated years' advent season
# For every year, if applicable, correct date range to not go beyond advent season
for year in range(date_start.year, date_end.year + 1):
  if year == date_start.year:
    if date_start < datetime(year, 12, 1):
      day_start = 1
    elif date_start <= datetime(year, 12, 25):
      day_start = date_start.day
    else:
      continue
    
    if date_end >= datetime(year, 12, 1) and date_end <= datetime(year, 12, 25):
      day_end = date_end.day #if date_end == today and now == today_est else date_end.day - 1
    elif date_end > datetime(year, 12, 25):
      day_end = 25

  elif year < date_end.year:
    day_start = 1
    day_end = 25
  
  else:
    if date_end >= datetime(year, 12, 1) and date_end <= datetime(year, 12, 25):
      day_start = 1
      day_end = date_end.day #if date_end == today and now == today_est else date_end.day - 1
    elif date_end > datetime(year, 12, 25):
      day_start = 1
      day_end = 25
    else:
      continue

  # If no arguments have been given, today's puzzle will be fetched
  # As local timezone may be ahead of the AoC creator's timezone, apply "fix" by setting back date(s) by 1 day
  if date_end == today and today_local < today_est:
    print(
      f"{bcolors.WARNING}"
      f"Your local timezone seems to be ahead of AoC creator's timezone. The puzzle for today may not be published yet."
      f"{bcolors.ENDC}"
    )
    day_end = day_end - 1
    if day_start > day_end:
      day_start = day_end

  # Create folder structure
  # YEAR
  # |- LANGUAGE (if specified)
  path = f"./{year}"
  if args.language is not None:
    path = f"{path}/{args.language}"

  os.makedirs(f"{path}", exist_ok=True)

  for day in range(day_start, day_end + 1):
    current_date = f"{year}-12-{day:02d}"

    print(
      f"{bcolors.OKGREEN}"
      f"Fetching information for {current_date}."
      f"{bcolors.ENDC}"
    )

    # Get Advent of Code site source and parse HTML document
    url = f"https://adventofcode.com/{year}/day/{day}"
    try:
      response = requests.get(url)

      if not response.status_code == requests.codes.ok: # pylint: disable=no-member
        response.raise_for_status()      

    except requests.exceptions.RequestException as e:
      print(
        f"{bcolors.FAIL}"
        f"Fetching information from {response.url} failed:\n{e}."
        f"{bcolors.ENDC}"
      )
      sys.exit(1)
    
    soup = BeautifulSoup(response.text, features = 'html.parser')
    aoc = soup.find('article', attrs={"class": "day-desc"})
    aoc_title = aoc.find('h2')
    aoc_title.name = 'h1'
    aoc_title.string = aoc_title.text.replace('---', '').strip()
    link = BeautifulSoup(f'<p><a href="{url}">View this puzzle on adventofcode.com</a> to submit your solution.</p>', features = 'html.parser')
    aoc.append(link)

    import pypandoc
    aoc = pypandoc.convert_text(str(aoc), 'md', format='html')

    # Create folder structure
    # YEAR
    # |- LANGUAGE (if specified)
    #    |- DATE
    #       |- README.md
    path_day = f"{path}/{current_date}"

    if not os.path.exists(path_day):
      os.makedirs(path_day)
    else:
      print(
        f"{bcolors.WARNING}"
        f"Path {path_day} already exists. Skipping"
        f"{bcolors.ENDC}"
      )
    
    with open(f"{path_day}/README.md", 'w') as fp:
      fp.write(str(aoc))
 
    # Wait a few seconds until the next request
    # https://www.reddit.com/r/adventofcode/comments/3v64sb/aoc_is_fragile_please_be_gentle/
    time.sleep(2)

