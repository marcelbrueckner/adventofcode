require 'rspec'
require_relative '../puzzle'

describe Puzzle do
  before { @puzzle = Puzzle.new('input.txt') }

  it "should calculate the total fuel requirements" do
    expect(@puzzle.solve()).to eq 3395944
  end
end
