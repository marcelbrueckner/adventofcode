class Puzzle

  def initialize (input_file)
    @input = File.readlines(input_file)
    @solution = 0
  end

  def solve
    @input.each do |mass|
      @solution += mass.to_i / 3 - 2
    end

    @solution
  end

end
